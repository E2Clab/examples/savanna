import zlib
import logging
import argparse
logging.basicConfig(level=logging.INFO)


def _compress(file):
    with open(file, "rb") as in_file:
        compressed = zlib.compress(in_file.read(), -1)
        print(f"original={in_file.tell()}")
    with open(file, "wb") as out_file:
        out_file.write(compressed)
        print(f"compressed={out_file.tell()}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="edge-worker")
    parser.add_argument(
        "--image",
        type=str,
        required=True,
        help="Image path.",
    )
    args = parser.parse_args()
    _compress(args.image)


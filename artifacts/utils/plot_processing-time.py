from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt


def get_delta_time(_file):
    file1 = open(_file, 'r')
    Lines = file1.readlines()
    _in = "intime:"
    _out = "predicttime:"  # predicttime: , outtime:
    FMT = '%H:%M:%S'
    in_time = []
    out_time = []
    delta_time = []
    for line in Lines:
        if line.__contains__(_in):
            in_time.append(line.replace("'\n", "").split(_in)[1])
        elif line.__contains__(_out):
            out_time.append(line.replace("\n", "").split(_out)[1])
    mymin = min([len(in_time), len(out_time)])
    print(f"min={mymin}")
    for i in range(mymin):
        if in_time[i] > out_time[i]:
            print(f"***** {in_time[i]}")
            print(f"***** {out_time[i]}")
            print(f"***** {(datetime.strptime(out_time[i], FMT) - datetime.strptime(in_time[i],FMT)).total_seconds()}")
        else:
            tdelta = (datetime.strptime(out_time[i], FMT) - datetime.strptime(in_time[i],FMT)).total_seconds()
            delta_time.append(tdelta)
    print(delta_time)
    return delta_time


def relative_difference(id, x1, x2):
    _min = np.min([x1, x2])
    _max = np.max([x1, x2])
    rel_diff = ((_max - _min) / _min)*100
    print(f"{id}: (({_max} - {_min}) / {_min})*100 = {rel_diff} (%)")
    return rel_diff


def percentage_difference(id, x1, x2):
    _min = np.min([x1, x2])
    _max = np.max([x1, x2])
    rel_diff = ((_max - _min) / ((_max + _min)/2))*100
    print(f"{id}: (({_max} - {_min}) / (({_max} + {_min})/2))*100 = {rel_diff}")
    return rel_diff


# FIXME Chameleon plot #####################################
# bar_color = ['#38761d', '#b6d7a8']
# FIXME Chameleon plot 15Kbit
# plotid = "15Kbit-chi"
# titleid = "15Kbit"
# delta_time_cloud = get_delta_time('15kbit/15kbit-100-30-cloud-chi.log')
# delta_time_edge = get_delta_time('15kbit/15kbit-100-30-edge-chi.log')
# yticks = [i*3 for i in range(5)]
# FIXME Chameleon plot 25Kbit
# plotid = "25Kbit-chi"
# titleid = "25Kbit"
# delta_time_cloud = get_delta_time('25kbit/25kbit-100-30-cloud-chi.log')
# delta_time_edge = get_delta_time('25kbit/25kbit-100-30-edge-chi.log')
# yticks = [i*3 for i in range(5)]
# FIXME G5K plot #####################################
bar_color = ['#b45f06', '#f9cb9c']
# FIXME G5K plot 15Kbit
# plotid = "15Kbit-g5k"
# titleid = "15Kbit"
# delta_time_cloud = get_delta_time('15kbit/15kbit-100-30-cloud-g5k.log')
# delta_time_edge = get_delta_time('15kbit/15kbit-100-30-edge-g5k.log')
# yticks = [i*3 for i in range(12)]
# FIXME G5K plot 25Kbit
plotid = "25Kbit-g5k"
titleid = "25Kbit"
delta_time_cloud = get_delta_time('25kbit/25kbit-100-30-cloud-g5k.log')
delta_time_edge = get_delta_time('25kbit/25kbit-100-30-edge-g5k.log')
yticks = [i*3 for i in range(12)]

print(f"len(delta_time_cloud)={len(delta_time_cloud)}")
print(f"len(delta_time_cloud)={len(delta_time_edge)}")

CTEs = [np.mean(delta_time_cloud), np.mean(delta_time_edge)]
error = [(np.std(delta_time_cloud)/np.sqrt(len(delta_time_cloud)))*1.96,
         (np.std(delta_time_edge)/np.sqrt(len(delta_time_edge)))*1.96]

SMALL_SIZE = 10
MEDIUM_SIZE = 12
BIGGER_SIZE = 20
fig, ax = plt.subplots()
materials = ['Cloud-centric', 'Edge+Cloud']
x_pos = np.arange(len(materials))
ax.bar(x_pos, CTEs, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10, color=bar_color)
ax.set_ylabel('Processing time (sec)', fontsize=BIGGER_SIZE)
ax.set_xticks(x_pos)
ax.set_xticklabels(materials, fontsize=BIGGER_SIZE)
ax.set_title(f'{titleid} Bandwidth', fontsize=BIGGER_SIZE)
ax.yaxis.grid(True)
ax.set_yticks(yticks)
ax.set_yticklabels(yticks, fontsize=BIGGER_SIZE)
plt.rc('ytick', labelsize=BIGGER_SIZE)
plt.tight_layout()
plt.savefig(f'plot_processing_time_{plotid}.pdf')
plt.show()
